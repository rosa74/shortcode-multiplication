<?php

/**
 * Plugin Name:       Mes tables de multiplication
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Rosa
 **/


add_action("get_footer", "add_my_phrase");
function add_my_phrase()
{
    echo 'BRAVO';
}

add_filter("the_content", "add_emoji");
function add_emoji($content)
{
    return '&#128299;' . $content;
}


add_shortcode("dice", "dice_shortcode");
function dice_shortcode()
{
    return rand(1, 6);
}
add_shortcode('table', 'multiplication');

function multiplication(){

/*$table = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];*/
$result = "";

    for ($i = 1; $i <= 10; $i++) {
               for ($j = 1; $j <= 10; $j++) {
                    $result.= $i . ' x ' . $j . ' = ' . $i * $j . '<br>';
        }
    /*foreach ($table as $nb) {
        foreach ($nombres as $nb2) {
            $result .= $nb . 'x' . $nb2 . '=' . ($nb * $nb2) . '<br>';
              
     }*/
    }return $result;
} 


?>